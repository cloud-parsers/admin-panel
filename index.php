<!DOCTYPE html>
<html>
  <head>
    <title>File Manager</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:wght@400;500;700&display=swap"
      rel="stylesheet"
    />
<style>
body {
	font-family: 'Roboto', sans-serif;
}

.curent-files-div {
	display: flex;
}
.curent-file-type-div:not(:first-child) {
	margin-left: 80px;
}
</style>
<style>
body {
	font-family: 'Roboto', sans-serif;
}

.curent-files-div {
	display: flex;
}
.curent-file-type-div:not(:first-child) {
	margin-left: 80px;
}
</style>
  </head>
  <body>
    <h1>File Manager</h1>
    
    <?php
	$dir = getenv("DIR") !== false ? getenv("DIR") : parse_ini_file("config.ini")["dir"];
      
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // file upload was submitted
        $file= $_FILES["file"]["name"]; // get uploaded file name
		$type=$_FILES["file"]["type"];
		if (str_starts_with($type,"image/"))
			$subdir="images";
		else
			$subdir = pathinfo($file, PATHINFO_EXTENSION); // get file extension as subdirectory name
        
        // create subdirectory if it doesn't exist
        if (!file_exists("$dir/$subdir")) {
          mkdir("$dir/$subdir", 0664, true);
        }
        
        // move uploaded file to the selected subdirectory
        move_uploaded_file($_FILES["file"]["tmp_name"], "$dir/$subdir/$file");
        
        // redirect back to main page
        header("Location: .");
      }
      else {
        // display file upload form
        echo "<h2>Upload New File</h2>";
        echo "<form action='index.php' method='post' enctype='multipart/form-data'>";
        echo "<label for='file'>Choose file:</label>";
        echo "<input type='file' name='file' id='file'><br>";
        echo "<input type='submit' name='submit' value='Upload'>";
        echo "</form>";

		// display list of current files
		echo "<h2>Current Files</h2>"; 
    echo "<div class='curent-files-div'>";
		$subdirs = array();
		$files = scandir($dir); // get list of files
		foreach ($files as $file) {
			if (!in_array($file, array(".", ".."))) {
				if (is_dir("$dir/$file")) {
					// display subdirectory name and files
					$subdir = $file;
					$subdirs[] = $subdir;
          echo "<div class='curent-file-type-div'>";
					echo "<h3>$subdir</h3>";
					echo "<ul>";
					$subfiles = scandir("$dir/$subdir");
					foreach ($subfiles as $subfile) {
						if (!in_array($subfile, array(".", ".."))) {
							echo "<li><a href='$subdir/$subfile'>$subfile</a></li>";
						}
					}
					echo "</ul>";
          echo "</div>";
				}
				else {
					// display file name
					echo "<li><a href='$file'>$file</a></li>";
				}
			}
		}
    echo "</div>";
	  }
?>

  </body>
</html>
